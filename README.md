# Le Tourment d'Isaac



## What is this ?

This is an attempt at localizing The Binding of Isaac, I started working on this a bit but, with unfortunate timing for me, I was not even near completion before Biobak uploaded his own take on it, La Ligature d'Isaac. I felt our translations were not distinct enough to warrant uploading mine to the workshop and his covered more content anyway.

As his mod was officially integrated with an update, I gave up continuing this. 
This covers the translation for curses, stages, character names, all handheld items (cards, pills, etc...),some challenges, some monsters, most bosses, and every item and trinket up to Rebirth. This does not cover items and trinkets released with Afterbirth, Afterbirth+ and Repentance, most of the monsters, achievements, and contains no graphical assets for challenge names, title screen, etc...

## Installation

- Download this repository.
- Navigate to your game folder, for steam this would look something like ```C:\Program Files (x86)\Steam\steamapps\common\The Binding of Isaac Rebirth\``` .
- Navigate to or create a ```mods``` folder and extract the archive you downloaded earlier.
    - In order to use mods in The Binding of Isaac it is necessary to have beaten Mom's foot at least once, a fully unlocked save file is available [here](https://github.com/Zamiell/isaac-save-installer/tree/main/saves/Repentance).

- In the menu, go to Mods and activate Le Tourment d'Isaac.
- To see items, navigate to the Stats menus, the Items. Nearly all the items for the first 3 pages are translated.
- Unfortunately there are no ways to look at trinket and handheld item names from the menu so you would need to go in game (see next step).
- In order to spawn specific items, open the console with ```~``` and use the command ```spawn``` followed by an ID.
Item and trinket IDs can be found [here](https://platinumgod.co.uk/) and cards and runes [here](https://bindingofisaacrebirth.fandom.com/wiki/Cards_and_Runes), the code for an item will always be ```5.100.xxx```, a trinket ```5.350.xxx``` and a card/rune ```5.300.xxx```.
For example, in order to spawn Magic Mushroom you would need to type ```spawn 5.100.12```.

## Screenshots

![Example 1](/example-1.jpg){: .shadow}
![Example 2](/example-2.jpg){: .shadow}
![Example 3](/example-3.jpg){: .shadow}
![Example 4](/example-4.jpg){: .shadow}